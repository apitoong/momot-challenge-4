//
//  Splash.swift
//  tes
//
//  Created by afitra mamor on 04/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation
import CoreData
import UIKit




class SplashController: UIViewController {
    
    
    @IBOutlet weak var logoLabel: UIImageView!
    
    
    override func viewDidLoad() {
       super.viewDidLoad()
      
        logoLabel.image=UIImage(named: "logo-fix")
        perform(#selector(moveScreen), with: nil, afterDelay: 2)
        
    }
    
//    @IBAction func getSegue(_ sender: Any) {
//        print("ok")
        
//          performSegue(withIdentifier: "toBoarding", sender: self)
    @objc func moveScreen() {
        
        let temp = Helpers().getSession()
        
        
        if(temp.count > 0){
            performSegue(withIdentifier: "toHome", sender: self)
        }else{
            performSegue(withIdentifier: "toBoarding", sender: self)
        }
    }
}
