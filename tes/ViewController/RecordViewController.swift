//
//  ViewRecordController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 01/06/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit
import AVFoundation

class RecordViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    var recordingSesstion: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    var numberOfRecords:Int = 0
    var objectName = ""
    
    var asyikFileName: [String] = []
    var bajuFileName: [String] = []
    var ranselFileName: [String] = []
    var tendaFileName: [String] = []

    @IBOutlet weak var recordsCollectionView: UICollectionView!
    
    @IBOutlet weak var recordButton: UIButton!
    
    @IBAction func recordDidTap(_ sender: Any) {
        if audioRecorder == nil {
            numberOfRecords += 1
            //saveObjekName = "\(objek)\(numberOfRecords)"
            let fileName = getDirectory().appendingPathComponent("\(objectName)\(numberOfRecords).m4a")
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 12000, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
            
            do {
                audioRecorder = try AVAudioRecorder(url: fileName, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
                
                recordButton.setImage(UIImage(named: "stopRecord"), for: .normal)
            }
            catch {
                displayAlert(title: "Oops!", message: "Rekamanmu gagal")
            }
        }
        else {
            audioRecorder.stop()
            audioRecorder = nil
            
            if objectName.lowercased() == "asyik" {
                asyikFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(asyikFileName, forKey: "asyik")
            }
            else if objectName.lowercased() == "baju" {
                bajuFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(bajuFileName, forKey: "baju")
            }
            else if objectName.lowercased() == "ransel" {
                ranselFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(ranselFileName, forKey: "ransel")
            }
            else if objectName.lowercased() == "tenda" {
                tendaFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(tendaFileName, forKey: "tenda")
            }
            
            recordButton.setImage(UIImage(named: "startRecord"), for: .normal)
            recordsCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPermission()
        
        recordsCollectionView.delegate = self
        recordsCollectionView.dataSource = self
    }
    
    func getDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        return documentDirectory
    }
    
    func displayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "dismiss", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func setupPermission() {
        recordingSesstion = AVAudioSession.sharedInstance()
        
        if let saveObject: [String] = UserDefaults.standard.object(forKey: "asyik") as? [String] {
            asyikFileName = saveObject
        }
        else if let saveObject: [String] = UserDefaults.standard.object(forKey: "baju") as? [String] {
            bajuFileName = saveObject
        }
        else if let saveObject: [String] = UserDefaults.standard.object(forKey: "ransel") as? [String] {
            ranselFileName = saveObject
        }
        else if let saveObject: [String] = UserDefaults.standard.object(forKey: "tenda") as? [String] {
            tendaFileName = saveObject
        }
        
        AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
            if hasPermission {
                print("ACCEPTED")
            }
        }
    }
}

extension RecordViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         print(objectName, "@@@@@@@")
        if objectName.lowercased() == "asyik" {
            return asyikFileName.count
        }
        else if objectName.lowercased() == "baju" {
            return bajuFileName.count
        }
        else if objectName.lowercased() == "ransel" {
            return ranselFileName.count
        }
        else if objectName.lowercased() == "tenda" {
            return tendaFileName.count
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM yyyy"
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        
        let cell = recordsCollectionView.dequeueReusableCell(withReuseIdentifier: "RecordCell", for: indexPath) as! RecordCell
        cell.nameRecordLabel.text = "Rekaman \(String(indexPath.row + 1))"
        cell.recordImage.image = UIImage(named: "rekaman")
        cell.recordTimeLabel.text = dateInFormat
        cell.deleteButton.setImage(UIImage(named: "hapus"), for: .normal)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if objectName.lowercased() == "asyik" {
            let path = getDirectory().appendingPathComponent("\(asyikFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        else if objectName.lowercased() == "baju" {
            let path = getDirectory().appendingPathComponent("\(bajuFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        else if objectName.lowercased() == "ransel" {
            let path = getDirectory().appendingPathComponent("\(ranselFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        else if objectName.lowercased() == "tenda" {
            let path = getDirectory().appendingPathComponent("\(tendaFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
    
    }
    
}

//    @IBOutlet weak var recordButton: UIButton!
//    @IBOutlet weak var collectionView: UICollectionView!
//
//    let stopRecordImage = UIImage(named: "stopRecord")
//    let startRecordImage = UIImage(named: "startRecord")
//    let normalButton = UIImage(named: "play")
//    let stopButton = UIImage(named: "stop")
//
//    var soundRecorder = AVAudioRecorder()
//    var soundPlayer = AVAudioPlayer()
//    var permisionrecord = AVAudioSession()
//
//    var numberOfRecords = 0
//    //var formatFile : String = ".m4a"
//    var namaBarang = ""
//
//
////    var dataDummy = []
//    var data1:[(name:String, record:[String])] = []
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //nameFile = "Objek\(numberOfRecords)\(formatFile)"
//
//        if let file: [(name:String, record:[String])] = UserDefaults.standard.object(forKey: "myNumber") as? [(name:String, record:[String])] {
//            data1 = file
//        }
//
//        setupPermission()
//        // Do any additional setup after loading the view.
//    }
//
//    func setupPermission() {
//        permisionrecord = AVAudioSession.sharedInstance()
//        do{
//            try permisionrecord.setCategory(.playAndRecord, mode: .default)
//            try permisionrecord.setActive(true)
//            permisionrecord.requestRecordPermission(){ [unowned self] allowed in
//                DispatchQueue.main.async {
//                    if allowed {
//
//                            self.setUpRecorder()
//                    } else {
//                        self.displayAlert(title: "Ups!", message: "Rekam suaramu gagal")
//                    }
//                }
//            }
//        }
//        catch{
//            self.displayAlert(title: "Ups!", message: "Rekam suaramu gagal")
//        }
//    }
//
//    func getDocumentDirector() -> URL {
//        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        return path[0]
//    }
//
//
////    func loadFailUI() {
////        let failLabel = UILabel()
////        failLabel.font = UIFont.preferredFont(forTextStyle: .headline)
////        failLabel.text = "Recording failed: please ensure the app has access to your microphone."
////        failLabel.numberOfLines = 0
////
////
////    }
//
//
//    func setUpRecorder() {
//        let audioFileName = getDocumentDirector().appendingPathComponent("\(namaBarang)-record-\(numberOfRecords).m4a")
//        print(audioFileName.absoluteString)
//        let recordSetting = [AVFormatIDKey: kAudioFormatAppleLossless,
//                             AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
//                             AVEncoderBitRateKey : 320000,
//                             AVNumberOfChannelsKey : 2,
//                             AVSampleRateKey : 44100]as [String : Any]
//
//        do{
//            soundRecorder = try AVAudioRecorder(url: audioFileName, settings: recordSetting)
//            soundRecorder.delegate = self
//            soundRecorder.prepareToRecord()
//        }
//        catch{
//            print(error)
//        }
//    }
//
////    func setUpPlayer() {
////        let audioFileName = getDocumentDirector().appendingPathComponent(nameFile)
////        do{
////            soundPlayer = try AVAudioPlayer(contentsOf: audioFileName)
////            soundPlayer.delegate = self
////            soundPlayer.prepareToPlay()
////            soundPlayer.volume = 3.0
////        }
////        catch{
////            print(error)
////        }
////    }
//
//
//
//    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
//        recordButton.isEnabled = true
//        //playRecordButton.setImage(normalButton, for: .normal)
//        //playRecordButton.setTitle("Play", for: .normal)
//    }
//
//    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
//        //playRecordButton.isEnabled = true
//    }
//
//    func displayAlert(title: String, message: String) {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
//    }
//
//
//
//    @IBAction func recordBtn(_ sender: Any) {
//
//            if recordButton.titleLabel?.text == "Record" {
//                numberOfRecords += 1
//                setUpRecorder()
//                recordButton.setImage(stopRecordImage, for: .normal)
//                recordButton.setTitle("Stop", for: .normal)
//                //playRecordButton.isEnabled = false
//
//        }
//            else{
//                soundRecorder.stop()
//                UserDefaults.standard.set(numberOfRecords, forKey: "myNumber")
//                recordButton.setImage(startRecordImage, for: .normal)
//                recordButton.setTitle("Record", for: .normal)
//                //playRecordButton.isEnabled = false
//        }
//
//    }
//
////    @IBAction func playBtn(_ sender: Any) {
////        if playRecordButton.titleLabel?.text == "Play" {
////            playRecordButton.setImage(stopButton, for: .normal)
////            playRecordButton.setTitle("Stop", for: .normal)
////            recordButton.isEnabled = false
////            setUpPlayer()
////            soundPlayer.play()
////        }
////        else{
////            soundPlayer.stop()
////            playRecordButton.setImage(normalButton, for: .normal)
////            playRecordButton.setTitle("Play", for: .normal)
////            recordButton.isEnabled = false
////
////        }
////    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return data1[section].record.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recordCell", for: indexPath) as! RecordCell
//        cell.labelRecord.text = data1[0].record[indexPath.item]
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let audioFileName = getDocumentDirector().appendingPathComponent( "\(namaBarang)-record-\(numberOfRecords).m4a")
//        do{
//            soundPlayer = try AVAudioPlayer(contentsOf: audioFileName)
//            soundPlayer.delegate = self
//            soundPlayer.play()
//            soundPlayer.volume = 3.0
//        }
//        catch{
//            print(error)
//        }
//    }

