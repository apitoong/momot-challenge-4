//
//  Boarding.swift
//  tes
//
//  Created by afitra mamor on 04/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation
import CoreData
import UIKit




class BoardingController: UIViewController {
    
 
    @IBOutlet weak var boardingTable: UITableView!
    
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
      super.viewDidLoad()
     
      boardingTable.delegate = self
      boardingTable.dataSource = self
        
      nextButton.backgroundColor = getUIColor(hex: "9961F5")
             nextButton.layer.cornerRadius = 30
        
    
  }
    var allFitur : [(title:String,description:String,image:String)] = [
        
        (title:"Membaca Bersama",description:"Baca bersama dan anak anda belajar mengeja dengan panduan yang telah disediakan",image:"buku")
        ,(title:"Tentukan Jalan Ceritamu",description:"Biarkan anak anda bebas memilih jalan ceritanya",image:"papan")
        ,(title:"Rekam Cara Mengeja",description:"Rekam dan simpan cara mengeja anak anda ",image:"ngomong"),
         (title:"Pilih Warna Disukai",description:"Kumpul kata dan pilih warna yang disukai",image:"warna")
        
    ]
    
    func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
            var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
            
            if (cleanString.hasPrefix("#")) {
                cleanString.remove(at: cleanString.startIndex)
            }
            
            if ((cleanString.count) != 6) {
                return nil
            }
            
            var rgbValue: UInt32 = 0
            Scanner(string: cleanString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
    
}


extension BoardingController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
       return  allFitur.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "fiturViewCell", for: indexPath) as! BoardingCell
                   
 
        cell.descriptionLabel.text = allFitur[indexPath.row].description
        cell.titleLabel.text = allFitur[indexPath.row].title
        cell.imageLabel.image = UIImage(named: allFitur[indexPath.row].image)
        return cell
        
    }
    
    
    
    
    
    
    
}
